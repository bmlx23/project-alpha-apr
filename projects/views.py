from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project_details = get_object_or_404(Project, id=id)
    context = {"project_details": project_details}
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        create = ProjectForm(request.POST)
        if create.is_valid():
            create.save(False)
            create.owner = request.user
            create.save()
            return redirect("list_projects")
    else:
        create = ProjectForm()
    context = {"create": create}
    return render(request, "projects/create.html", context)
