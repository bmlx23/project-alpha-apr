from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

# Create your views here.


def user_login(request):
    if request.method == "POST":
        userLogin = LoginForm(request.POST)
        if userLogin.is_valid():
            username = userLogin.cleaned_data["username"]
            password = userLogin.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        userLogin = LoginForm()
    context = {"userLogin": userLogin}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        signup = SignupForm(request.POST)
        if signup.is_valid():
            username = signup.cleaned_data["username"]
            password = signup.cleaned_data["password"]
            password_confirmation = signup.cleaned_data[
                "password_confirmation"
            ]

            if password == password_confirmation:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("list_projects")
            else:
                signup.add_error("password", "the passwords do not match")
    else:
        signup = SignupForm()
    context = {"signup": signup}
    return render(request, "registration/signup.html", context)
