from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        task = TaskForm(request.POST)
        if task.is_valid():
            task.save()
            return redirect("list_projects")
    else:
        task = TaskForm()
    context = {"task": task}
    return render(request, "tasks/create.html", context)


@login_required
def list_task(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/list.html", context)
